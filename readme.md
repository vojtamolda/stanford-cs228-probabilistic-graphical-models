# Stanford Course CS228 - Probabilistic Graphical Models (Fall 2016)


Lecture videos, exams, assignments and other materials can be downloaded from the [course webpage](http://cs.stanford.edu/~ermon/cs228/index.html) or [coursera](https://www.coursera.org/learn/probabilistic-graphical-models/home/welcome).


## Prerequisites

Background in basic probability theory, statistics, programming, algorithm design and analysis.


## Course Description

Probabilistic graphical models are a powerful framework for representing complex domains using probability distributions, with numerous applications in machine learning, computer vision, natural language processing and computational biology. Graphical models bring together graph theory and probability theory, and provide a flexible framework for modeling large collections of random variables with complex interactions. This course will provide a comprehensive survey of the topic, introducing the key formalisms and main techniques used to construct them, make predictions, and support decision-making under uncertainty.

The aim of this course is to develop the knowledge and skills necessary to design, implement and apply these models to solve real problems. The course will cover: (1) Bayesian networks, undirected graphical models and their temporal extensions; (2) exact and approximate inference methods; (3) estimation of the parameters and the structure of graphical models.


## Textbook
Daphne Koller and Nir Friedman: [_Probabilistic Graphical Models: Principles and Techniques_](https://www.amazon.com/Probabilistic-Graphical-Models-Principles-Computation/dp/0262013193)
